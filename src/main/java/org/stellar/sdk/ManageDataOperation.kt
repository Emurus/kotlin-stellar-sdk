package org.stellar.sdk


import org.stellar.sdk.xdr.DataValue
import org.stellar.sdk.xdr.ManageDataOp
import org.stellar.sdk.xdr.OperationType
import org.stellar.sdk.xdr.String64

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [ManageData](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#manage-data) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class ManageDataOperation private constructor(val name: String, val value: ByteArray?) : Operation() {

    internal override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = ManageDataOp().apply {
            dataName = String64().apply { string64 = name }
        }

        value?.let {
            op.dataValue = DataValue().apply { dataValue = this@ManageDataOperation.value }
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.MANAGE_DATA
            manageDataOp = op
        }
    }

    class Builder(private val name: String, private val value: ByteArray?) {

        private var mSourceAccount: KeyPair? = null

        /**
         * Construct a new ManageOffer builder from a ManageDataOp XDR.
         * @param op [ManageDataOp]
         */
        internal constructor(op: ManageDataOp):this(
                op.dataName.string64!!,
                op.dataValue?.dataValue
        )

        /**
         * Sets the source account for this operation.
         * @param sourceAccount The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): ManageDataOperation {
            val operation = ManageDataOperation(name, value)
            if (mSourceAccount != null) {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
