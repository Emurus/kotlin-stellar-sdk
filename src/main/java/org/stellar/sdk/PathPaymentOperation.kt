package org.stellar.sdk


import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.Int64
import org.stellar.sdk.xdr.OperationType
import org.stellar.sdk.xdr.PathPaymentOp

import com.google.common.base.Preconditions.checkArgument
import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [PathPayment](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#path-payment) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class PathPaymentOperation private constructor(sendAsset: Asset, sendMax: String, destination: KeyPair,
                                               destAsset: Asset, destAmount: String, path: Array<Asset?>?) : Operation() {


    /**
     * The asset deducted from the sender's account.
     */
    val sendAsset: Asset
    /**
     * The maximum amount of send asset to deduct (excluding fees)
     */
    val sendMax: String
    /**
     * Account that receives the payment.
     */
    val destination: KeyPair
    /**
     * The asset the destination account receives.
     */
    val destAsset: Asset
    /**
     * The amount of destination asset the destination account receives.
     */
    val destAmount: String
    /**
     * The assets (other than send asset and destination asset) involved in the offers the path takes. For example, if you can only find a path from USD to EUR through XLM and BTC, the path would be USD - XLM - BTC - EUR and the path would contain XLM and BTC.
     */
    val path: Array<Asset?>

    init {
        this.sendAsset = checkNotNull(sendAsset, "sendAsset cannot be null")
        this.sendMax = checkNotNull(sendMax, "sendMax cannot be null")
        this.destination = checkNotNull(destination, "destination cannot be null")
        this.destAsset = checkNotNull(destAsset, "destAsset cannot be null")
        this.destAmount = checkNotNull(destAmount, "destAmount cannot be null")
        if (path == null) {
            this.path = arrayOfNulls(0)
        } else {
            checkArgument(path.size <= 5, "The maximum number of assets in the path is 5")
            this.path = path
        }
    }

    override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = PathPaymentOp().apply {
            // sendAsset
            sendAsset = this@PathPaymentOperation.sendAsset.toXdr()
            // sendMax
            sendMax = Int64().apply { int64 = Operation.toXdrAmount(this@PathPaymentOperation.sendMax) }
            // destination
            destination = AccountID().apply { accountID = this@PathPaymentOperation.destination.xdrPublicKey }
            // destAsset
            destAsset = this@PathPaymentOperation.destAsset.toXdr()
            // destAmount
            destAmount = Int64().apply { int64 = Operation.toXdrAmount(this@PathPaymentOperation.destAmount) }
        }

        // path
        val path = arrayOfNulls<org.stellar.sdk.xdr.Asset>(this.path.size)
        for (i in this.path.indices) {
            path[i] = this.path[i]?.toXdr()
        }
        op.path = path

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.PATH_PAYMENT
            pathPaymentOp = op
        }
    }

    /**
     * Builds PathPayment operation.
     * @see PathPaymentOperation
     */
    class Builder(private val sendAsset: Asset, private val sendMax: String, private val destination: KeyPair,
                  private val destAsset: Asset, private val destAmount: String) {

        private var path: Array<Asset?>? = null

        private var mSourceAccount: KeyPair? = null

        internal constructor(op: PathPaymentOp):this(
                Asset.fromXdr(op.sendAsset),
                Operation.fromXdrAmount(op.sendMax.int64!!.toLong()),
                KeyPair.fromXdrPublicKey(op.destination.accountID!!),
                Asset.fromXdr(op.destAsset),
                Operation.fromXdrAmount(op.destAmount.int64!!.toLong())) {

            // Didn't attempt to change this. It works fine.
            path = arrayOfNulls(op.path.size)
            for (i in 0 until op.path.size) {
                path!![i] = Asset.fromXdr(op.path[i])
            }
        }

        /**
         * Sets path for this operation
         * @param path The assets (other than send asset and destination asset) involved in the offers the path takes. For example, if you can only find a path from USD to EUR through XLM and BTC, the path would be USD - XLM - BTC - EUR and the path field would contain XLM and BTC.
         * @return Builder object so you can chain methods.
         */
        fun setPath(path: Array<Asset?>): Builder {
            checkArgument(path.size <= 5, "The maximum number of assets in the path is 5")
            this.path = path
            return this
        }

        /**
         * Sets the source account for this operation.
         * @param sourceAccount The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): PathPaymentOperation {
            val operation = PathPaymentOperation(
                    sendAsset, sendMax, destination,
                    destAsset, destAmount, path)

            mSourceAccount?.let {
                operation.sourceAccount = mSourceAccount
            }

            return operation
        }
    }
}
