package org.stellar.sdk


import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.AllowTrustOp
import org.stellar.sdk.xdr.AssetType
import org.stellar.sdk.xdr.OperationType

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [AllowTrust](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#allow-trust) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class AllowTrustOperation private constructor(
        val trustor: KeyPair,
        val assetCode: String,
        val authorize: Boolean) : Operation() {

    internal override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = AllowTrustOp()

        // trustor
        op.trustor = AccountID().apply { accountID = trustor.xdrPublicKey }

        // asset
        op.asset = AllowTrustOp.AllowTrustOpAsset().apply {
            if (assetCode.length <= 4) {
                discriminant = AssetType.ASSET_TYPE_CREDIT_ALPHANUM4
                assetCode4 = Util.paddedByteArray(assetCode, 4)
            } else {
                discriminant = AssetType.ASSET_TYPE_CREDIT_ALPHANUM12
                assetCode12 = Util.paddedByteArray(assetCode, 12)
            }
        }

        // authorize
        op.authorize = authorize

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.ALLOW_TRUST
            allowTrustOp = op
        }
    }

    /**
     * Creates a new AllowTrust builder.
     * @param trustor The account of the recipient of the trustline.
     * @param assetCode The asset of the trustline the source account is authorizing. For example, if a gateway wants to allow another account to hold its USD credit, the type is USD.
     * @param authorize Flag indicating whether the trustline is authorized.
     */
    class Builder(private val trustor: KeyPair, private val assetCode: String, private val authorize: Boolean) {

        private var mSourceAccount: KeyPair? = null

        internal constructor(op: AllowTrustOp):this(
                KeyPair.fromXdrPublicKey(op.trustor.accountID!!),
                when (op.asset.discriminant) {
                    AssetType.ASSET_TYPE_CREDIT_ALPHANUM4 -> String(op.asset.assetCode4).trim { it <= ' ' }
                    AssetType.ASSET_TYPE_CREDIT_ALPHANUM12 -> String(op.asset.assetCode12).trim { it <= ' ' }
                    else -> throw RuntimeException("Unknown asset code")
                },
                op.authorize!!
        )

        /**
         * Set source account of this operation
         * @param sourceAccount Source account
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): AllowTrustOperation {
            val operation = AllowTrustOperation(trustor, assetCode, authorize)
            if (mSourceAccount != null) {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
