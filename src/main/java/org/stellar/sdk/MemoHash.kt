package org.stellar.sdk

import org.stellar.sdk.xdr.Hash
import org.stellar.sdk.xdr.Memo
import org.stellar.sdk.xdr.MemoType

/**
 * Represents MEMO_HASH.
 */
class MemoHash : MemoHashAbstract {
    constructor(bytes: ByteArray) : super(bytes) {}

    constructor(hexString: String) : super(hexString) {}

    override fun toXdr(): org.stellar.sdk.xdr.Memo {
        return Memo().apply {
            discriminant = MemoType.MEMO_HASH
            hash = Hash().apply { hash = bytes }
        }
    }
}
