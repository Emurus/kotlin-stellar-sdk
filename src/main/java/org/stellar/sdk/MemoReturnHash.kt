package org.stellar.sdk

import org.stellar.sdk.xdr.Memo
import org.stellar.sdk.xdr.MemoType

/**
 * Represents MEMO_RETURN.
 */
class MemoReturnHash : MemoHashAbstract {
    constructor(bytes: ByteArray) : super(bytes) {}

    constructor(hexString: String) : super(hexString) {}

    override fun toXdr(): Memo {
        return org.stellar.sdk.xdr.Memo().apply {
            discriminant = MemoType.MEMO_RETURN
            hash = org.stellar.sdk.xdr.Hash().apply { hash = this@MemoReturnHash.bytes }
        }
    }
}
