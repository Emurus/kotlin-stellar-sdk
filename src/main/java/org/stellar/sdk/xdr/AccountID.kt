package org.stellar.sdk.xdr

import java.io.IOException

// === xdr source ============================================================

//  typedef PublicKey AccountID;

//  ===========================================================================
class AccountID {
    var accountID: PublicKey? = null

    companion object {
        @Throws(IOException::class)
        fun encode(stream: XdrDataOutputStream, encodedAccountID: AccountID) {
            PublicKey.encode(stream, encodedAccountID.accountID)
        }

        @Throws(IOException::class)
        fun decode(stream: XdrDataInputStream): AccountID {
            return AccountID().apply { accountID = PublicKey.decode(stream) }
        }
    }
}