package org.stellar.sdk.xdr

import java.io.IOException

// === xdr source ============================================================

//  typedef string string64<64>;

//  ===========================================================================
class String64 {
    var string64: String? = null

    companion object {
        @Throws(IOException::class)
        fun encode(stream: XdrDataOutputStream, encodedString64: String64) = stream.writeString(encodedString64.string64)

        @Throws(IOException::class)
        fun decode(stream: XdrDataInputStream): String64 = String64().apply { string64 = stream.readString() }
    }
}