package org.stellar.sdk.xdr

import java.io.IOException

// === xdr source ============================================================

//  struct AccountEntry
//  {
//      AccountID accountID;      // master public key for this account
//      int64 balance;            // in stroops
//      SequenceNumber seqNum;    // last sequence number used for this account
//      uint32 numSubEntries;     // number of sub-entries this account has
//                                // drives the reserve
//      AccountID* inflationDest; // Account to vote for during inflation
//      uint32 flags;             // see AccountFlags
//
//      string32 homeDomain; // can be used for reverse federation and memo lookup
//
//      // fields used for signatures
//      // thresholds stores unsigned bytes: [weight of master|low|medium|high]
//      Thresholds thresholds;
//
//      Signer signers<20>; // possible signers for this account
//
//      // reserved for future use
//      union switch (int v)
//      {
//      case 0:
//          void;
//      }
//      ext;
//  };

//  ===========================================================================
class AccountEntry {
    lateinit var accountID: AccountID
    lateinit var balance: Int64
    lateinit var seqNum: SequenceNumber
    lateinit var numSubEntries: Uint32
    var inflationDest: AccountID? = null
    lateinit var flags: Uint32
    lateinit var homeDomain: String32
    lateinit var thresholds: Thresholds
    lateinit var signers: Array<Signer?>
    lateinit var ext: AccountEntryExt

    class AccountEntryExt {
        var discriminant: Int = -1

        companion object {
            @Throws(IOException::class)
            fun encode(stream: XdrDataOutputStream, encodedAccountEntryExt: AccountEntryExt) = stream.writeInt(encodedAccountEntryExt.discriminant)

            @Throws(IOException::class)
            fun decode(stream: XdrDataInputStream): AccountEntryExt = AccountEntryExt().apply {
                discriminant = stream.readInt()
            }

        }

    }

    companion object {
        @Throws(IOException::class)
        fun encode(stream: XdrDataOutputStream, encodedAccountEntry: AccountEntry) {
            AccountID.encode(stream, encodedAccountEntry.accountID)
            Int64.encode(stream, encodedAccountEntry.balance)
            SequenceNumber.encode(stream, encodedAccountEntry.seqNum)
            Uint32.encode(stream, encodedAccountEntry.numSubEntries)
            if (encodedAccountEntry.inflationDest != null) {
                stream.writeInt(1)
                AccountID.encode(stream, encodedAccountEntry.inflationDest!!)
            } else {
                stream.writeInt(0)
            }
            Uint32.encode(stream, encodedAccountEntry.flags)
            String32.encode(stream, encodedAccountEntry.homeDomain)
            Thresholds.encode(stream, encodedAccountEntry.thresholds)
            val signersSize = encodedAccountEntry.signers.size
            stream.writeInt(signersSize)
            for (i in 0 until signersSize) {
                Signer.encode(stream, encodedAccountEntry.signers[i])
            }
            AccountEntryExt.encode(stream, encodedAccountEntry.ext)
        }

        @Throws(IOException::class)
        fun decode(stream: XdrDataInputStream): AccountEntry {
            val decodedAccountEntry = AccountEntry().apply {
                accountID = AccountID.decode(stream)
                balance = Int64.decode(stream)
                seqNum = SequenceNumber.decode(stream)
                numSubEntries = Uint32.decode(stream)
            }

            val inflationDestPresent = stream.readInt()
            if (inflationDestPresent != 0) {
                decodedAccountEntry.inflationDest = AccountID.decode(stream)
            }
            decodedAccountEntry.flags = Uint32.decode(stream)
            decodedAccountEntry.homeDomain = String32.decode(stream)
            decodedAccountEntry.thresholds = Thresholds.decode(stream)
            val signersSize = stream.readInt()
            decodedAccountEntry.signers = arrayOfNulls(signersSize)
            for (i in 0 until signersSize) {
                decodedAccountEntry.signers[i] = Signer.decode(stream)
            }
            decodedAccountEntry.ext = AccountEntryExt.decode(stream)
            return decodedAccountEntry
        }
    }
}