package org.stellar.sdk.xdr

import java.io.IOException

// === xdr source ============================================================

//  typedef string string32<32>;

//  ===========================================================================
class String32 {
    var string32: String? = null

    companion object {
        @Throws(IOException::class)
        fun encode(stream: XdrDataOutputStream, encodedString32: String32) {
            stream.writeString(encodedString32.string32)
        }

        @Throws(IOException::class)
        fun decode(stream: XdrDataInputStream): String32 {
            val decodedString32 = String32()
            decodedString32.string32 = stream.readString()
            return decodedString32
        }
    }
}