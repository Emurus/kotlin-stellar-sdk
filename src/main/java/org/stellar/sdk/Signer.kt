package org.stellar.sdk

import org.stellar.sdk.xdr.SignerKey
import org.stellar.sdk.xdr.SignerKeyType
import org.stellar.sdk.xdr.Uint256

/**
 * Signer is a helper class that creates [org.stellar.sdk.xdr.SignerKey] objects.
 */
object Signer {
   /**
    * Create `ed25519PublicKey` [org.stellar.sdk.xdr.SignerKey] from
    * a [org.stellar.sdk.KeyPair]
    * @param keyPair
    * @return org.stellar.sdk.xdr.SignerKey
    */
   fun ed25519PublicKey(keyPair: KeyPair): SignerKey {
      return keyPair.xdrSignerKey
   }

   /**
    * Create `sha256Hash` [org.stellar.sdk.xdr.SignerKey] from
    * a sha256 hash of a preimage.
    * @param hash
    * @return org.stellar.sdk.xdr.SignerKey
    */
   fun sha256Hash(hash: ByteArray): SignerKey {
      val value = Signer.createUint256(hash)
      return SignerKey().apply {
         discriminant = SignerKeyType.SIGNER_KEY_TYPE_HASH_X
         hashX = value
      }
   }

   /**
    * Create `preAuthTx` [org.stellar.sdk.xdr.SignerKey] from
    * a [org.stellar.sdk.xdr.Transaction] hash.
    * @param tx
    * @return org.stellar.sdk.xdr.SignerKey
    */
   fun preAuthTx(tx: Transaction): SignerKey {
      val value = Signer.createUint256(tx.hash())
      return SignerKey().apply {
         discriminant = SignerKeyType.SIGNER_KEY_TYPE_PRE_AUTH_TX
         preAuthTx = value
      }
   }

   /**
    * Create `preAuthTx` [org.stellar.sdk.xdr.SignerKey] from
    * a transaction hash.
    * @param hash
    * @return org.stellar.sdk.xdr.SignerKey
    */
   fun preAuthTx(hash: ByteArray): SignerKey {
      val value = Signer.createUint256(hash)
      return SignerKey().apply {
         discriminant = SignerKeyType.SIGNER_KEY_TYPE_PRE_AUTH_TX
         preAuthTx = value
      }
   }

   private fun createUint256(hash: ByteArray): Uint256 {
      if (hash.size != 32) {
         throw RuntimeException("hash must be 32 bytes long")
      }
      return Uint256().apply { uint256 = hash }
   }
}
