package org.stellar.sdk


import org.stellar.sdk.xdr.CreateAccountOp
import org.stellar.sdk.xdr.Int64
import org.stellar.sdk.xdr.ManageOfferOp
import org.stellar.sdk.xdr.OperationType
import org.stellar.sdk.xdr.Uint64

import java.math.BigDecimal

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [ManageOffer](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#manage-offer) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class ManageOfferOperation private constructor(
        val selling: Asset,
        val buying: Asset,
        val amount: String,
        val price: String,
        val offerId: Long) : Operation() {

    internal override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = ManageOfferOp().apply {
            selling = this@ManageOfferOperation.selling.toXdr()
            buying = this@ManageOfferOperation.buying.toXdr()
            amount = Int64().apply { int64 = Operation.toXdrAmount(this@ManageOfferOperation.amount)  }
            price = Price.fromString(this@ManageOfferOperation.price).toXdr()
            offerID = Uint64().apply { uint64 = java.lang.Long.valueOf(this@ManageOfferOperation.offerId) }
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.MANAGE_OFFER
            manageOfferOp = op
        }
    }

    /**
     * Builds ManageOffer operation. If you want to update existing offer use
     * [org.stellar.sdk.ManageOfferOperation.Builder.setOfferId].
     * @see ManageOfferOperation
     */
    class Builder @JvmOverloads constructor(private val selling: Asset, private val buying: Asset, private val amount: String, private val price: String, private var offerId: Long = 0L) {

        private var mSourceAccount: KeyPair? = null

        /**
         * Construct a new CreateAccount builder from a CreateAccountOp XDR.
         * @param op [CreateAccountOp]
         */
        internal constructor(op: ManageOfferOp):this(
                Asset.fromXdr(op.selling),
                Asset.fromXdr(op.buying),
                Operation.fromXdrAmount(op.amount.int64!!),
                BigDecimal(op.price.n.int32!!).divide(BigDecimal(op.price.d.int32!!)).toString(),
                op.offerID.uint64!!
        )

        /**
         * Sets offer ID. `0` creates a new offer. Set to existing offer ID to change it.
         * @param offerId
         */
        fun setOfferId(offerId: Long): Builder {
            this.offerId = offerId
            return this
        }

        /**
         * Sets the source account for this operation.
         * @param sourceAccount The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): ManageOfferOperation {
            val operation = ManageOfferOperation(selling, buying, amount, price, offerId)
            if (mSourceAccount != null) {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
