package org.stellar.sdk

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents an account in Stellar network with it's sequence number.
 * Account object is required to build a [Transaction].
 * @see org.stellar.sdk.Transaction.Builder
 */

/**
 * Class constructor.
 * @param keypair KeyPair associated with this Account
 * @param sequenceNumber Current sequence number of the account (can be obtained using java-stellar-sdk or horizon server)
 */
class Account(override val keypair: KeyPair, private var seqNumber: Long): TransactionBuilderAccount {

    /**
     * Returns current sequence number ot this Account.
     */
    override val sequenceNumber: Long
    get() = seqNumber

    /**
     * Returns sequence number incremented by one, but does not increment internal counter.
     */
    override val incrementedSequenceNumber: Long
        get() = seqNumber + 1

    /**
     * Increments sequence number in this object by one.
     */
    override fun incrementSequenceNumber() {
        seqNumber++
    }
}
