package org.stellar.sdk


import org.stellar.sdk.xdr.MemoType
import org.stellar.sdk.xdr.Uint64

/**
 * Represents MEMO_ID.
 */
class MemoId(val id: Long) : Memo() {

    init {
        if (id < 0) {
            throw IllegalArgumentException("id must be a positive number")
        }
    }

    override fun toXdr(): org.stellar.sdk.xdr.Memo {
        val memo = org.stellar.sdk.xdr.Memo().apply {
            discriminant = MemoType.MEMO_ID
            id = Uint64().apply { uint64 = this@MemoId.id }
        }
        return memo
    }
}
