package org.stellar.sdk


import org.stellar.sdk.xdr.*

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [SetOptions](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#set-options) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class SetOptionsOperation private constructor(
        /**
         * Account of the inflation destination.
         */
        val inflationDestination: KeyPair?,
        /**
         * Indicates which flags to clear. For details about the flags, please refer to the [accounts doc](https://www.stellar.org/developers/learn/concepts/accounts.html).
         * You can also use [AccountFlag] enum.
         */
        val clearFlags: Int?,
        /**
         * Indicates which flags to set. For details about the flags, please refer to the [accounts doc](https://www.stellar.org/developers/learn/concepts/accounts.html).
         * You can also use [AccountFlag] enum.
         */
        val setFlags: Int?,
        /**
         * Weight of the master key.
         */
        val masterKeyWeight: Int?,
        /**
         * A number from 0-255 representing the threshold this account sets on all operations it performs that have [a low threshold](https://www.stellar.org/developers/learn/concepts/multi-sig.html).
         */
        val lowThreshold: Int?,
        /**
         * A number from 0-255 representing the threshold this account sets on all operations it performs that have [a medium threshold](https://www.stellar.org/developers/learn/concepts/multi-sig.html).
         */
        val mediumThreshold: Int?,
        /**
         * A number from 0-255 representing the threshold this account sets on all operations it performs that have [a high threshold](https://www.stellar.org/developers/learn/concepts/multi-sig.html).
         */
        val highThreshold: Int?,
        /**
         * The home domain of an account.
         */
        val homeDomain: String?,
        /**
         * Additional signer added/removed in this operation.
         */
        val signer: SignerKey?,
        /**
         * Additional signer weight. The signer is deleted if the weight is 0.
         */
        val signerWeight: Int?) : Operation() {

    override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = SetOptionsOp()
        if (inflationDestination != null) {
            op.inflationDest = AccountID().apply { accountID = this@SetOptionsOperation.inflationDestination.xdrPublicKey }
        }
        if (clearFlags != null) {
            op.clearFlags = Uint32().apply { uint32 = this@SetOptionsOperation.clearFlags }
        }
        if (setFlags != null) {
            op.setFlags = Uint32().apply { uint32 = this@SetOptionsOperation.setFlags }
        }
        if (masterKeyWeight != null) {
            op.masterWeight = Uint32().apply { uint32 = this@SetOptionsOperation.masterKeyWeight }
        }
        if (lowThreshold != null) {
            op.lowThreshold = Uint32().apply { uint32 = this@SetOptionsOperation.lowThreshold }
        }
        if (mediumThreshold != null) {
            op.medThreshold = Uint32().apply { uint32 = this@SetOptionsOperation.mediumThreshold }
        }
        if (highThreshold != null) {
            op.highThreshold = Uint32().apply { uint32 = this@SetOptionsOperation.highThreshold }
        }
        if (homeDomain != null) {
            op.homeDomain = String32().apply { string32 = this@SetOptionsOperation.homeDomain }
        }
        if (signer != null) {
            val signer = org.stellar.sdk.xdr.Signer().apply {
                weight = Uint32().apply { uint32 = this@SetOptionsOperation.signerWeight!! and 0xFF }
                key = this@SetOptionsOperation.signer
            }

            op.signer = signer
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.SET_OPTIONS
            setOptionsOp = op
        }
    }

    /**
     * Builds SetOptions operation.
     * @see SetOptionsOperation
     */
    class Builder {
        private var inflationDestination: KeyPair? = null
        private var clearFlags: Int? = null
        private var setFlags: Int? = null
        private var masterKeyWeight: Int? = null
        private var lowThreshold: Int? = null
        private var mediumThreshold: Int? = null
        private var highThreshold: Int? = null
        private var homeDomain: String? = null
        private var signer: SignerKey? = null
        private var signerWeight: Int? = null
        private var sourceAccount: KeyPair? = null

        internal constructor(op: SetOptionsOp) {
            op.inflationDest?.let {
                inflationDestination = KeyPair.fromXdrPublicKey(op.inflationDest.accountID!!)
            }
            op.clearFlags?.let {
                clearFlags = op.clearFlags.uint32
            }
            op.setFlags?.let {
                setFlags = op.setFlags.uint32
            }
            op.masterWeight?.let {
                masterKeyWeight = op.masterWeight.uint32!!.toInt()
            }
            op.lowThreshold?.let {
                lowThreshold = op.lowThreshold.uint32!!.toInt()
            }
            op.medThreshold?.let {
                mediumThreshold = op.medThreshold.uint32!!.toInt()
            }
            op.highThreshold?.let {
                highThreshold = op.highThreshold.uint32!!.toInt()
            }
            op.homeDomain?.let {
                homeDomain = op.homeDomain.string32
            }
            op.signer?.let {
                signer = op.signer.key
                signerWeight = op.signer.weight.uint32!!.toInt() and 0xFF
            }
        }

        /**
         * Creates a new SetOptionsOperation builder.
         */
        constructor() {}

        /**
         * Sets the inflation destination for the account.
         * @param inflationDestination The inflation destination account.
         * @return Builder object so you can chain methods.
         */
        fun setInflationDestination(inflationDestination: KeyPair): Builder {
            this.inflationDestination = inflationDestination
            return this
        }

        /**
         * Clears the given flags from the account.
         * @param clearFlags For details about the flags, please refer to the [accounts doc](https://www.stellar.org/developers/learn/concepts/accounts.html).
         * @return Builder object so you can chain methods.
         */
        fun setClearFlags(clearFlags: Int): Builder {
            this.clearFlags = clearFlags
            return this
        }

        /**
         * Sets the given flags on the account.
         * @param setFlags For details about the flags, please refer to the [accounts doc](https://www.stellar.org/developers/learn/concepts/accounts.html).
         * @return Builder object so you can chain methods.
         */
        fun setSetFlags(setFlags: Int): Builder {
            this.setFlags = setFlags
            return this
        }

        /**
         * Weight of the master key.
         * @param masterKeyWeight Number between 0 and 255
         * @return Builder object so you can chain methods.
         */
        fun setMasterKeyWeight(masterKeyWeight: Int): Builder {
            this.masterKeyWeight = masterKeyWeight
            return this
        }

        /**
         * A number from 0-255 representing the threshold this account sets on all operations it performs that have a low threshold.
         * @param lowThreshold Number between 0 and 255
         * @return Builder object so you can chain methods.
         */
        fun setLowThreshold(lowThreshold: Int): Builder {
            this.lowThreshold = lowThreshold
            return this
        }

        /**
         * A number from 0-255 representing the threshold this account sets on all operations it performs that have a medium threshold.
         * @param mediumThreshold Number between 0 and 255
         * @return Builder object so you can chain methods.
         */
        fun setMediumThreshold(mediumThreshold: Int): Builder {
            this.mediumThreshold = mediumThreshold
            return this
        }

        /**
         * A number from 0-255 representing the threshold this account sets on all operations it performs that have a high threshold.
         * @param highThreshold Number between 0 and 255
         * @return Builder object so you can chain methods.
         */
        fun setHighThreshold(highThreshold: Int): Builder {
            this.highThreshold = highThreshold
            return this
        }

        /**
         * Sets the account's home domain address used in [Federation](https://www.stellar.org/developers/learn/concepts/federation.html).
         * @param homeDomain A string of the address which can be up to 32 characters.
         * @return Builder object so you can chain methods.
         */
        fun setHomeDomain(homeDomain: String): Builder {
            if (homeDomain.length > 32) {
                throw IllegalArgumentException("Home domain must be <= 32 characters")
            }
            this.homeDomain = homeDomain
            return this
        }

        /**
         * Add, update, or remove a signer from the account. Signer is deleted if the weight = 0;
         * @param signer The signer key. Use [org.stellar.sdk.Signer] helper to create this object.
         * @param weight The weight to attach to the signer (0-255).
         * @return Builder object so you can chain methods.
         */
        fun setSigner(signer: SignerKey, weight: Int): Builder {
            this.signer = signer
            this.signerWeight = weight and 0xFF
            return this
        }

        /**
         * Sets the source account for this operation.
         * @param sourceAccount The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            this.sourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): SetOptionsOperation {
            val operation = SetOptionsOperation(inflationDestination, clearFlags,
                    setFlags, masterKeyWeight, lowThreshold, mediumThreshold, highThreshold,
                    homeDomain, signer, signerWeight)
            sourceAccount?.let {
                operation.sourceAccount = sourceAccount
            }
            return operation
        }
    }
}
