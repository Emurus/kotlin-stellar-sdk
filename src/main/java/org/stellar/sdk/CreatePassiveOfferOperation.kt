package org.stellar.sdk


import org.stellar.sdk.xdr.CreatePassiveOfferOp
import org.stellar.sdk.xdr.Int64
import org.stellar.sdk.xdr.OperationType

import java.math.BigDecimal

/**
 * Represents [CreatePassiveOffer](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#create-passive-offer) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class CreatePassiveOfferOperation private constructor(val selling: Asset, val buying: Asset, val amount: String, val price: String) : Operation() {

    internal override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = CreatePassiveOfferOp().apply {
            selling = this@CreatePassiveOfferOperation.selling.toXdr()
            buying = this@CreatePassiveOfferOperation.buying.toXdr()
            amount = Int64().apply { int64 = Operation.toXdrAmount(this@CreatePassiveOfferOperation.amount)  }
            price = Price.fromString(this@CreatePassiveOfferOperation.price).toXdr()
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.CREATE_PASSIVE_OFFER
            createPassiveOfferOp = op
        }
    }

    /**
     * Builds CreatePassiveOffer operation.
     * @see CreatePassiveOfferOperation
     */
    class Builder(private val selling: Asset, private val buying: Asset, private val amount: String, private val price: String) {
        private var mSourceAccount: KeyPair? = null

        /**
         * Construct a new CreatePassiveOffer builder from a CreatePassiveOfferOp XDR.
         * @param op
         */
        internal constructor(op: CreatePassiveOfferOp):this(
                Asset.fromXdr(op.selling),
                Asset.fromXdr(op.buying),
                Operation.fromXdrAmount(op.amount.int64),
                BigDecimal(op.price.n.int32).divide(BigDecimal(op.price.d.int32)).toString()
        )

        /**
         * Sets the source account for this operation.
         * @param sourceAccount The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): CreatePassiveOfferOperation {
            val operation = CreatePassiveOfferOperation(selling, buying, amount, price)
            mSourceAccount?.let {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
