package org.stellar.sdk

import org.stellar.sdk.xdr.MemoType

/**
 * Represents MEMO_NONE.
 */
class MemoNone : Memo() {
    override fun toXdr(): org.stellar.sdk.xdr.Memo {
        val memo = org.stellar.sdk.xdr.Memo().apply {
            discriminant = MemoType.MEMO_NONE
        }
        return memo
    }
}
