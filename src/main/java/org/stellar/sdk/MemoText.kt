package org.stellar.sdk


import org.stellar.sdk.xdr.MemoType

import java.nio.charset.StandardCharsets

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents MEMO_TEXT.
 */
class MemoText(val text: String) : Memo() {
    init {
        val length = text.toByteArray(StandardCharsets.UTF_8).size
        if (length > 28) {
            throw MemoTooLongException("text must be <= 28 bytes. length=$length)")
        }
    }

    override fun toXdr(): org.stellar.sdk.xdr.Memo {
        val memo = org.stellar.sdk.xdr.Memo().apply {
            discriminant = MemoType.MEMO_TEXT
            text = this@MemoText.text
        }
        return memo
    }
}