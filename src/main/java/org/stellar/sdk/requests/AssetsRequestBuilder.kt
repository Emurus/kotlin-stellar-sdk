package org.stellar.sdk.requests

import com.google.gson.reflect.TypeToken
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.stellar.sdk.responses.Page
import org.stellar.sdk.responses.AssetResponse

import java.io.IOException

class AssetsRequestBuilder(httpClient: OkHttpClient, serverURI: HttpUrl) : RequestBuilder(httpClient, serverURI, "assets") {

   fun assetCode(assetCode: String): AssetsRequestBuilder {
      uriBuilder.setQueryParameter("asset_code", assetCode)
      return this
   }

   fun assetIssuer(assetIssuer: String): AssetsRequestBuilder {
      uriBuilder.setQueryParameter("asset_issuer", assetIssuer)
      return this
   }

   @Throws(IOException::class, TooManyRequestsException::class)
   fun execute(): Page<AssetResponse>? {
      return AssetsRequestBuilder.execute(this.httpClient, this.buildUri())
   }

   companion object {

      @Throws(IOException::class, TooManyRequestsException::class)
      fun execute(httpClient: OkHttpClient, uri: HttpUrl): Page<AssetResponse>? {
         val type = object : TypeToken<Page<AssetResponse>>() {

         }
         val responseHandler = ResponseHandler<Page<AssetResponse>>(type)

         val request = Request.Builder().get().url(uri).build()
         val response = httpClient.newCall(request).execute()

         return responseHandler.handleResponse(response)
      }
   }
}
