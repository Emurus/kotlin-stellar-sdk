package org.stellar.sdk.requests;

/**
 * Exception thrown when request returned an non-success HTTP code.
 */
class ErrorResponse(val code: Int, val body: String) : RuntimeException("Error response from the server.")
