package org.stellar.sdk.requests

import com.google.gson.reflect.TypeToken
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.stellar.sdk.Asset
import org.stellar.sdk.AssetTypeCreditAlphaNum
import org.stellar.sdk.responses.Page
import org.stellar.sdk.responses.TradeAggregationResponse

import java.io.IOException

/**
 * Builds requests connected to trades.
 */
class TradeAggregationsRequestBuilder(httpClient: OkHttpClient, serverURI: HttpUrl, baseAsset: Asset, counterAsset: Asset, startTime: Long, endTime: Long, resolution: Long) : RequestBuilder(httpClient, serverURI, "trade_aggregations") {
   init {

      this.baseAsset(baseAsset)
      this.counterAsset(counterAsset)
      uriBuilder.setQueryParameter("start_time", startTime.toString())
      uriBuilder.setQueryParameter("end_time", endTime.toString())
      uriBuilder.setQueryParameter("resolution", resolution.toString())
   }

   private fun baseAsset(asset: Asset) {
      uriBuilder.setQueryParameter("base_asset_type", asset.type)
      if (asset is AssetTypeCreditAlphaNum) {
         val creditAlphaNumAsset = asset
         uriBuilder.setQueryParameter("base_asset_code", creditAlphaNumAsset.code)
         uriBuilder.setQueryParameter("base_asset_issuer", creditAlphaNumAsset.issuer.accountId)
      }
   }

   private fun counterAsset(asset: Asset) {
      uriBuilder.setQueryParameter("counter_asset_type", asset.type)
      if (asset is AssetTypeCreditAlphaNum) {
         val creditAlphaNumAsset = asset
         uriBuilder.setQueryParameter("counter_asset_code", creditAlphaNumAsset.code)
         uriBuilder.setQueryParameter("counter_asset_issuer", creditAlphaNumAsset.issuer.accountId)
      }
   }

   @Throws(IOException::class, TooManyRequestsException::class)
   fun execute(): Page<TradeAggregationResponse>? {
      return TradeAggregationsRequestBuilder.execute(this.httpClient, this.buildUri())
   }

   companion object {

      @Throws(IOException::class, TooManyRequestsException::class)
      fun execute(httpClient: OkHttpClient, uri: HttpUrl): Page<TradeAggregationResponse>? {
         val type = object : TypeToken<Page<TradeAggregationResponse>>() {

         }
         val responseHandler = ResponseHandler<Page<TradeAggregationResponse>>(type)

         val request = Request.Builder().get().url(uri).build()
         val response = httpClient.newCall(request).execute()

         return responseHandler.handleResponse(response)
      }
   }
}
