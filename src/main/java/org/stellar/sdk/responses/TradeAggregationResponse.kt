package org.stellar.sdk.responses

import com.google.gson.annotations.SerializedName

import java.util.Date

class TradeAggregationResponse(
        @field:SerializedName("timestamp") val timestamp: Long,
        @field:SerializedName("trade_count") val tradeCount: Int,
        @field:SerializedName("base_volume") val baseVolume: String,
        @field:SerializedName("counter_volume") val counterVolume: String,
        @field:SerializedName("avg") val avg: String,
        @field:SerializedName("high") val high: String,
        @field:SerializedName("low") val low: String,
        @field:SerializedName("open") val open: String,
        @field:SerializedName("close") val close: String) : Response() {

   val date: Date
      get() = Date(java.lang.Long.valueOf(this.timestamp)!!)
}
