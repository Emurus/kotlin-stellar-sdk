package org.stellar.sdk.responses

import com.google.gson.annotations.SerializedName
import org.stellar.sdk.Asset

class AssetResponse(@field:SerializedName("asset_type")
                    val assetType: String, @field:SerializedName("asset_code")
                    val assetCode: String, @field:SerializedName("asset_issuer")
                    val assetIssuer: String, @field:SerializedName("paging_token")
                    val pagingToken: String, @field:SerializedName("amount")
                    val amount: String, @field:SerializedName("num_accounts")
                    val numAccounts: Int, flags: Flags, links: Links) : Response() {
   @SerializedName("flags")
   val flags: Flags
   @SerializedName("_links")
   val links: Links

   val asset: Asset
      get() = Asset.create(this.assetType, this.assetCode, this.assetIssuer)

   init {
      this.flags = flags
      this.links = links
   }

   /**
    * Flags describe asset flags.
    */
   class Flags(@field:SerializedName("auth_required")
               val isAuthRequired: Boolean, @field:SerializedName("auth_revocable")
               val isAuthRevocable: Boolean)

   /**
    * Links connected to asset.
    */
   class Links(@field:SerializedName("toml")
               val toml: Link)
}
