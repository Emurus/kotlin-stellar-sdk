package org.stellar.sdk.responses


import com.google.gson.annotations.SerializedName

import org.stellar.sdk.Asset
import org.stellar.sdk.AssetTypeNative
import org.stellar.sdk.KeyPair

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents account response.
 * @see [Account documentation](https://www.stellar.org/developers/horizon/reference/resources/account.html)
 *
 * @see org.stellar.sdk.requests.AccountsRequestBuilder
 *
 * @see org.stellar.sdk.Server.accounts
 */
class AccountResponse(
        @field:SerializedName("account_id") override val keypair: KeyPair,
        @field:SerializedName("sequence") private var seqNumber: Long)
    : Response(), org.stellar.sdk.TransactionBuilderAccount {

    override val sequenceNumber: Long
        get() = seqNumber

    @SerializedName("paging_token")
    lateinit var pagingToken: String
        private set

    @SerializedName("subentry_count")
    var subentryCount: Int = -1
        private set

    @SerializedName("inflation_destination")
    lateinit var inflationDestination: String
        private set

    @SerializedName("home_domain")
    lateinit var homeDomain: String
        private set

    @SerializedName("thresholds")
    lateinit var thresholds: Thresholds
        private set

    @SerializedName("flags")
    lateinit var flags: Flags
        private set

    @SerializedName("balances")
    lateinit var balances: Array<Balance>
        private set

    @SerializedName("signers")
    lateinit var signers: Array<Signer?>
        private set

    @SerializedName("_links")
    lateinit var links: Links
        private set

    override val incrementedSequenceNumber: Long
        get() = seqNumber + 1

    override fun incrementSequenceNumber() {
        seqNumber++
    }

    /**
     * Represents account thresholds.
     */
    class Thresholds internal constructor(
            @field:SerializedName("low_threshold") val lowThreshold: Int,
            @field:SerializedName("med_threshold") val medThreshold: Int,
            @field:SerializedName("high_threshold") val highThreshold: Int)

    /**
     * Represents account flags.
     */
    class Flags internal constructor(
            @field:SerializedName("auth_required") val authRequired: Boolean,
            @field:SerializedName("auth_revocable") val authRevocable: Boolean)

    /**
     * Represents account balance.
     */
    class Balance internal constructor(
            @field:SerializedName("asset_type") val assetType: String,
            @field:SerializedName("asset_code") val assetCode: String,
            @field:SerializedName("asset_issuer") private val assetIssuer: String,
            @field:SerializedName("balance") val balance: String,
            @field:SerializedName("limit") val limit: String) {

        val asset: Asset
            get() = if (assetType == "native") {
                AssetTypeNative()
            } else {
                Asset.createNonNativeAsset(assetCode, getAssetIssuer())
            }

        fun getAssetIssuer(): KeyPair {
            return KeyPair.fromAccountId(assetIssuer)
        }
    }

    /**
     * Represents account signers.
     */
    class Signer internal constructor(
            @field:SerializedName("public_key") val accountId: String,
            @field:SerializedName("weight") val weight: Int)

    /**
     * Links connected to account.
     */
    class Links internal constructor(
            @field:SerializedName("effects") val effects: Link,
            @field:SerializedName("offers") val offers: Link,
            @field:SerializedName("operations") val operations: Link,
            @field:SerializedName("self") val self: Link,
            @field:SerializedName("transactions") val transactions: Link)
}
