package org.stellar.sdk

import org.stellar.sdk.xdr.TimeBounds
import org.stellar.sdk.xdr.Uint64

/**
 *
 * TimeBounds represents the time interval that a transaction is valid.
 * @see Transaction
 */
class TimeBounds
/**
 * @param minTime 64bit Unix timestamp
 * @param maxTime 64bit Unix timestamp
 */
(val minTime: Long, val maxTime: Long) {

   init {
      if (minTime > maxTime) {
         throw IllegalArgumentException("minTime must be <= maxTime")
      }
   }

   fun toXdr(): org.stellar.sdk.xdr.TimeBounds {
      return TimeBounds().apply {
         minTime = Uint64().apply { uint64 = this@TimeBounds.minTime }
         maxTime = Uint64().apply { uint64 = this@TimeBounds.maxTime }
      }
   }
}
