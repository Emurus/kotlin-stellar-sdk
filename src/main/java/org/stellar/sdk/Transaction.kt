package org.stellar.sdk

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.util.ArrayList
import java.util.Arrays
import java.util.Collections

import com.google.common.base.Preconditions.checkArgument
import org.stellar.sdk.xdr.*
import com.google.common.io.BaseEncoding



/**
 * Represents [Transaction](https://www.stellar.org/developers/learn/concepts/transactions.html) in Stellar network.
 */
class Transaction internal constructor(val sourceAccount: KeyPair,
                                       val sequenceNumber: Long,
                                       private val mOperations: Array<Operation>,
                                       memo: Memo?,
                                       val timeBounds: TimeBounds?) {
   private val BASE_FEE = 100
   val fee: Int
   val memo: Memo
   private val mSignatures: MutableList<DecoratedSignature>
   val signatures: List<DecoratedSignature>
      get() = mSignatures

   init {
      checkArgument(mOperations.isNotEmpty(), "At least one operation required")
      fee = mOperations.size * BASE_FEE
      mSignatures = mutableListOf()
      this.memo = memo ?: Memo.none()
   }

   /**
    * Adds a new signature ed25519PublicKey to this transaction.
    * @param signer [KeyPair] object representing a signer
    */
   fun sign(signer: KeyPair) {
      val txHash = this.hash()
      mSignatures.add(signer.signDecorated(txHash))
   }

   /**
    * Adds a new sha256Hash signature to this transaction by revealing preimage.
    * @param preimage the sha256 hash of preimage should be equal to signer hash
    */
   fun sign(preimage: ByteArray) {
      val decoratedSignature = DecoratedSignature().apply {
         val hash = Util.hash(preimage)
         val signatureHintBytes = Arrays.copyOfRange(hash, hash.size - 4, hash.size)

         this.hint = SignatureHint().apply { this.signatureHint = signatureHintBytes }
         this.signature = Signature().apply { this.signature = preimage }
      }

      mSignatures.add(decoratedSignature)
   }

   /**
    * Returns transaction hash.
    */
   fun hash(): ByteArray {
      return Util.hash(this.signatureBase())
   }

   /**
    * Returns signature base.
    */
   fun signatureBase(): ByteArray? {
      if (Network.current() == null) {
         throw NoNetworkSelectedException()
      }

      try {
         val outputStream = ByteArrayOutputStream()
         // Hashed NetworkID
         outputStream.write(Network.current()!!.networkId)
         // Envelope Type - 4 bytes
         outputStream.write(ByteBuffer.allocate(4).putInt(EnvelopeType.ENVELOPE_TYPE_TX.value).array())
         // Transaction XDR bytes
         val txOutputStream = ByteArrayOutputStream()
         val xdrOutputStream = XdrDataOutputStream(txOutputStream)
         org.stellar.sdk.xdr.Transaction.encode(xdrOutputStream, this.toXdr())
         outputStream.write(txOutputStream.toByteArray())

         return outputStream.toByteArray()
      } catch (exception: IOException) {
         return null
      }

   }

   /**
    * Generates Transaction XDR object.
    */
   fun toXdr(): org.stellar.sdk.xdr.Transaction {
      // operations
      val operations = arrayOfNulls<org.stellar.sdk.xdr.Operation>(mOperations.size)
      for (i in mOperations.indices) {
         operations[i] = mOperations[i].toXdr()
      }

      val transaction = org.stellar.sdk.xdr.Transaction().apply {
         this.fee = org.stellar.sdk.xdr.Uint32().apply { uint32 = this@Transaction.fee }

         val sequenceNumberUint = org.stellar.sdk.xdr.Uint64().apply { uint64 = this@Transaction.sequenceNumber }
         this.seqNum = org.stellar.sdk.xdr.SequenceNumber().apply { sequenceNumber = sequenceNumberUint }

         this.sourceAccount = AccountID().apply { accountID = this@Transaction.sourceAccount.xdrPublicKey }
         this.operations = operations
         this.memo = this@Transaction.memo.toXdr()
         this.timeBounds = this@Transaction.timeBounds?.toXdr()
         this.ext = org.stellar.sdk.xdr.Transaction.TransactionExt().apply { discriminant = 0 }
      }

      return transaction
   }

   /**
    * Generates TransactionEnvelope XDR object. Transaction need to have at least one signature.
    */
   fun toEnvelopeXdr(): org.stellar.sdk.xdr.TransactionEnvelope {
      if (mSignatures.size == 0) {
         //throw NotEnoughSignaturesException("Transaction must be signed by at least one signer. Use transaction.sign().")
      }

      val xdr = org.stellar.sdk.xdr.TransactionEnvelope().apply {
         tx = this@Transaction.toXdr()
         signatures = mSignatures.toTypedArray()
      }

      return xdr
   }

   /**
    * Returns base64-encoded TransactionEnvelope XDR object. Transaction need to have at least one signature.
    */
   fun toEnvelopeXdrBase64(): String {
      try {
         val envelope = this.toEnvelopeXdr()
         val outputStream = ByteArrayOutputStream()
         val xdrOutputStream = XdrDataOutputStream(outputStream)
         org.stellar.sdk.xdr.TransactionEnvelope.encode(xdrOutputStream, envelope)

         val base64Encoding = BaseEncoding.base64()
         return base64Encoding.encode(outputStream.toByteArray())
      } catch (e: IOException) {
         throw AssertionError(e)
      }
   }

   companion object {
      fun fromXdr(xdrTransaction: org.stellar.sdk.xdr.Transaction): Transaction {
         val xdrMemo = xdrTransaction.memo
         val memo = when(xdrMemo.discriminant){
            MemoType.MEMO_HASH -> Memo.hash(xdrMemo.hash.hash)
            MemoType.MEMO_ID -> Memo.id(xdrMemo.id.uint64)
            MemoType.MEMO_RETURN -> Memo.returnHash(xdrMemo.retHash.hash)
            MemoType.MEMO_TEXT -> Memo.text(xdrMemo.text)
            MemoType.MEMO_NONE, null -> Memo.none()
         }

         var timeBound:TimeBounds? = null
         if(xdrTransaction.timeBounds != null){
            val xdrTimeBound = xdrTransaction.timeBounds
            timeBound = TimeBounds(xdrTimeBound.minTime.uint64, xdrTimeBound.maxTime.uint64)
         }

         return Transaction(
                 KeyPair.fromXdrPublicKey(xdrTransaction.sourceAccount.accountID!!),
                 xdrTransaction.seqNum.sequenceNumber.uint64,
                 xdrTransaction.operations.map { Operation.fromXdr(it) }.toTypedArray(),
                 memo, timeBound)
      }
   }


   /**
    * Builds a new Transaction object.
    */
   class Builder
   /**
    * Construct a new transaction builder.
    * @param sourceAccount The source account for this transaction. This account is the account
    * who will use a sequence number. When build() is called, the account object's sequence number
    * will be incremented.
    */
   (private val mSourceAccount: TransactionBuilderAccount) {
      private var mMemo: Memo? = null
      private var mTimeBounds: TimeBounds? = null
      internal var mOperations: MutableList<Operation> = Collections.synchronizedList(ArrayList())

      val operationsCount: Int
         get() = mOperations.size

      /**
       * Adds a new [operation](https://www.stellar.org/developers/learn/concepts/list-of-operations.html) to this transaction.
       * @param operation
       * @return Builder object so you can chain methods.
       * @see Operation
       */
      fun addOperation(operation: Operation): Builder {
         mOperations.add(operation)
         return this
      }

      /**
       * Adds a [memo](https://www.stellar.org/developers/learn/concepts/transactions.html) to this transaction.
       * @param memo
       * @return Builder object so you can chain methods.
       * @see Memo
       */
      fun addMemo(memo: Memo): Builder {
         if (mMemo != null) {
            throw RuntimeException("Memo has been already added.")
         }
         mMemo = memo
         return this
      }

      /**
       * Adds a [time-bounds](https://www.stellar.org/developers/learn/concepts/transactions.html) to this transaction.
       * @param timeBounds
       * @return Builder object so you can chain methods.
       * @see TimeBounds
       */
      fun addTimeBounds(timeBounds: TimeBounds): Builder {
         if (mTimeBounds != null) {
            throw RuntimeException("TimeBounds has been already added.")
         }
         mTimeBounds = timeBounds
         return this
      }

      /**
       * Builds a transaction. It will increment sequence number of the source account.
       */
      fun build(): Transaction {
         var operations = mOperations.toTypedArray()
         val transaction = Transaction(mSourceAccount.keypair, mSourceAccount.incrementedSequenceNumber, operations, mMemo, mTimeBounds)
         // Increment sequence number when there were no exceptions when creating a transaction
         mSourceAccount.incrementSequenceNumber()
         return transaction
      }
   }
}
