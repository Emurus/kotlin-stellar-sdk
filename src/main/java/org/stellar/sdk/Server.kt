package org.stellar.sdk

import okhttp3.*
import org.stellar.sdk.requests.*
import org.stellar.sdk.responses.GsonSingleton
import org.stellar.sdk.responses.SubmitTransactionResponse

import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * Main class used to connect to Horizon server.
 */
class Server(uri: String) {
   private val serverURI: HttpUrl
   var httpClient: OkHttpClient

   init {
      serverURI = HttpUrl.parse(uri)!!
      httpClient = OkHttpClient.Builder()
              .connectTimeout(10, TimeUnit.SECONDS)
              .readTimeout(30, TimeUnit.SECONDS)
              .retryOnConnectionFailure(false)
              .build()
   }

   /**
    * Returns [AccountsRequestBuilder] instance.
    */
   fun accounts(): AccountsRequestBuilder {
      return AccountsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [AssetsRequestBuilder] instance.
    */
   fun assets(): AssetsRequestBuilder {
      return AssetsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [EffectsRequestBuilder] instance.
    */
   fun effects(): EffectsRequestBuilder {
      return EffectsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [LedgersRequestBuilder] instance.
    */
   fun ledgers(): LedgersRequestBuilder {
      return LedgersRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [OffersRequestBuilder] instance.
    */
   fun offers(): OffersRequestBuilder {
      return OffersRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [OperationsRequestBuilder] instance.
    */
   fun operations(): OperationsRequestBuilder {
      return OperationsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [OrderBookRequestBuilder] instance.
    */
   fun orderBook(): OrderBookRequestBuilder {
      return OrderBookRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [TradesRequestBuilder] instance.
    */
   fun trades(): TradesRequestBuilder {
      return TradesRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [TradeAggregationsRequestBuilder] instance.
    */
   fun tradeAggregations(baseAsset: Asset, counterAsset: Asset, startTime: Long, endTime: Long, resolution: Long): TradeAggregationsRequestBuilder {
      return TradeAggregationsRequestBuilder(httpClient, serverURI, baseAsset, counterAsset, startTime, endTime, resolution)
   }

   /**
    * Returns [PathsRequestBuilder] instance.
    */
   fun paths(): PathsRequestBuilder {
      return PathsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [PaymentsRequestBuilder] instance.
    */
   fun payments(): PaymentsRequestBuilder {
      return PaymentsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Returns [TransactionsRequestBuilder] instance.
    */
   fun transactions(): TransactionsRequestBuilder {
      return TransactionsRequestBuilder(httpClient, serverURI)
   }

   /**
    * Submits transaction to the network.
    * @param transaction transaction to submit to the network.
    * @return [SubmitTransactionResponse]
    * @throws IOException
    */
   @Throws(IOException::class)
   fun submitTransaction(transaction: Transaction): SubmitTransactionResponse {
      val transactionsURI = serverURI!!.newBuilder().addPathSegment("transactions").build()
      val requestBody = FormBody.Builder().add("tx", transaction.toEnvelopeXdrBase64()).build()
      val submitTransactionRequest = Request.Builder().url(transactionsURI).post(requestBody).build()

      var response: Response? = null
      try {
         response = this.httpClient!!.newCall(submitTransactionRequest).execute()
         val submitTransactionResponse = GsonSingleton.getInstance().fromJson(response!!.body()!!.string(), SubmitTransactionResponse::class.java)
         return submitTransactionResponse
      } finally {
         if (response != null) {
            response.close()
         }
      }
   }
}
