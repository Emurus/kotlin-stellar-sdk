package org.stellar.sdk

import com.google.common.io.BaseEncoding

abstract class MemoHashAbstract(bytes: ByteArray) : Memo() {
    /**
     * Returns 32 bytes long array contained in this memo.
     */
    var bytes: ByteArray
        protected set

    /**
     *
     * Returns hex representation of bytes contained in this memo.
     *
     *
     * Example:
     * `
     * MemoHash memo = new MemoHash("4142434445");
     * memo.getHexValue(); // 4142434445000000000000000000000000000000000000000000000000000000
     * memo.getTrimmedHexValue(); // 4142434445
    ` *
     */
    val hexValue: String
        get() = BaseEncoding.base16().lowerCase().encode(this.bytes)

    /**
     *
     * Returns hex representation of bytes contained in this memo until null byte (0x00) is found.
     *
     *
     * Example:
     * `
     * MemoHash memo = new MemoHash("4142434445");
     * memo.getHexValue(); // 4142434445000000000000000000000000000000000000000000000000000000
     * memo.getTrimmedHexValue(); // 4142434445
    ` *
     */
    val trimmedHexValue: String
        get() = this.hexValue.split("00".toRegex())[0]

    init {
        when {
           bytes.size < 32 -> this.bytes = Util.paddedByteArray(bytes, 32)
           bytes.size > 32 -> throw MemoTooLongException("MEMO_HASH can contain 32 bytes at max.")
           else -> this.bytes = bytes
        }
    }

    // We change to lowercase because we want to decode both: upper cased and lower cased alphabets.
    constructor(hexString: String) : this(BaseEncoding.base16().lowerCase().decode(hexString.toLowerCase())){}

    abstract override fun toXdr(): org.stellar.sdk.xdr.Memo
}
