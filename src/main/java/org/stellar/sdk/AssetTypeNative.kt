package org.stellar.sdk

import org.stellar.sdk.xdr.AssetType


/**
 * Represents Stellar native asset - [lumens (XLM)](https://www.stellar.org/developers/learn/concepts/assets.html)
 * @see [Assets](https://www.stellar.org/developers/learn/concepts/assets.html)
 */
class AssetTypeNative : Asset() {

    override val type: String = "native"

    override fun equals(other: Any?): Boolean = this.javaClass == other!!.javaClass

    override fun hashCode(): Int = 0

    override fun toXdr(): org.stellar.sdk.xdr.Asset {
        return org.stellar.sdk.xdr.Asset().apply{
            discriminant = AssetType.ASSET_TYPE_NATIVE
        }
    }
}
