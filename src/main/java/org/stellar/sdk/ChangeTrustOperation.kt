package org.stellar.sdk


import org.stellar.sdk.xdr.ChangeTrustOp
import org.stellar.sdk.xdr.Int64
import org.stellar.sdk.xdr.OperationType

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [ChangeTrust](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#change-trust) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class ChangeTrustOperation private constructor(val asset: Asset, val limit: String) : Operation() {

    internal override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = ChangeTrustOp().apply {
            line = asset.toXdr()
            limit = Int64().apply {
                int64 = Operation.toXdrAmount(this@ChangeTrustOperation.limit)
            }
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.CHANGE_TRUST
            changeTrustOp = op
        }
    }

    /**
     * Creates a new ChangeTrust builder.
     * @param asset The asset of the trustline. For example, if a gateway extends a trustline of up to 200 USD to a user, the line is USD.
     * @param limit The limit of the trustline. For example, if a gateway extends a trustline of up to 200 USD to a user, the limit is 200.
     * @throws ArithmeticException when limit has more than 7 decimal places.
     */
    class Builder(private val asset: Asset, private val limit: String) {

        private var mSourceAccount: KeyPair? = null

        internal constructor(op: ChangeTrustOp):this(
                Asset.fromXdr(op.line),
                Operation.fromXdrAmount(op.limit.int64!!.toLong())
        )

        /**
         * Set source account of this operation
         * @param sourceAccount Source account
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): ChangeTrustOperation {
            val operation = ChangeTrustOperation(asset, limit)
            mSourceAccount?.let {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
