package org.stellar.sdk


import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.AssetType

/**
 * Represents all assets with codes 1-4 characters long.
 * @see [Assets](https://www.stellar.org/developers/learn/concepts/assets.html)
 */
class AssetTypeCreditAlphaNum4
/**
 * Class constructor
 * @param code Asset code
 * @param issuer Asset issuer
 */
(code: String, issuer: KeyPair) : AssetTypeCreditAlphaNum(code, issuer) {

    override val type: String
        get() = "credit_alphanum4"

    init {
        if (code.length < 1 || code.length > 4) {
            throw AssetCodeLengthInvalidException()
        }
    }

    override fun toXdr(): org.stellar.sdk.xdr.Asset {
        val accountID = AccountID().apply { accountID = mIssuer.xdrPublicKey  }
        val credit = org.stellar.sdk.xdr.Asset.AssetAlphaNum4().apply {
            assetCode = Util.paddedByteArray(code, 4)
            issuer = accountID
        }
        val xdr = org.stellar.sdk.xdr.Asset().apply {
            discriminant = AssetType.ASSET_TYPE_CREDIT_ALPHANUM4
            alphaNum4 = credit
        }

        return xdr
    }
}