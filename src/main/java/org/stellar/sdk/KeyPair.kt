package org.stellar.sdk


import net.i2p.crypto.eddsa.EdDSAEngine
import net.i2p.crypto.eddsa.EdDSAPrivateKey
import net.i2p.crypto.eddsa.EdDSAPublicKey
import net.i2p.crypto.eddsa.KeyPairGenerator
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable
import net.i2p.crypto.eddsa.spec.EdDSAPrivateKeySpec
import net.i2p.crypto.eddsa.spec.EdDSAPublicKeySpec

import org.stellar.sdk.xdr.*

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.security.GeneralSecurityException
import java.security.MessageDigest
import java.security.SignatureException
import java.util.Arrays

import com.google.common.base.Preconditions.checkNotNull

/**
 * Holds a Stellar keypair.
 */
class KeyPair
/**
 * Creates a new KeyPair from the given public and private keys.
 * @param publicKey
 * @param privateKey
 */
@JvmOverloads constructor(private val mPublicKey: EdDSAPublicKey, private val mPrivateKey: EdDSAPrivateKey? = null) {

    /**
     * Returns the human readable account ID encoded in strkey.
     */
    val accountId: String = StrKey.encodeStellarAccountId(mPublicKey.abyte)

    /**
     * Returns the human readable secret seed encoded in strkey.
     */
    val secretSeed: CharArray
        get() = StrKey.encodeStellarSecretSeed(mPrivateKey!!.seed)

    val publicKey: ByteArray = mPublicKey.abyte

    val signatureHint: SignatureHint
        get() {
            try {
                val publicKeyBytesStream = ByteArrayOutputStream()
                val xdrOutputStream = XdrDataOutputStream(publicKeyBytesStream)
                PublicKey.encode(xdrOutputStream, this.xdrPublicKey)
                val publicKeyBytes = publicKeyBytesStream.toByteArray()
                val signatureHintBytes = publicKeyBytes.drop(publicKeyBytes.size-4)

                return SignatureHint().apply { signatureHint = signatureHintBytes.toByteArray() }
            } catch (e: IOException) {
                throw AssertionError(e)
            }

        }

    val xdrPublicKey: PublicKey
        get() {
            return PublicKey().apply {
                discriminant = PublicKeyType.PUBLIC_KEY_TYPE_ED25519
                ed25519 = Uint256().apply { uint256 = this@KeyPair.publicKey }
            }
        }

    val xdrSignerKey: SignerKey
        get(){
            return SignerKey().apply {
                discriminant = SignerKeyType.SIGNER_KEY_TYPE_ED25519
                ed25519 = Uint256().apply { uint256 = this@KeyPair.publicKey }
            }
        }


    /**
     * Returns true if this Keypair is capable of signing
     */
    fun canSign(): Boolean = (mPrivateKey != null)

    /**
     * Sign the provided data with the keypair's private key.
     * @param data The data to sign.
     * @return signed bytes, null if the private key for this keypair is null.
     */
    fun sign(data: ByteArray): ByteArray {
        if (mPrivateKey == null) {
            throw RuntimeException("KeyPair does not contain secret key. Use KeyPair.fromSecretSeed method to create a new KeyPair with a secret key.")
        }
        try {
            val sgr = EdDSAEngine(MessageDigest.getInstance("SHA-512"))
            sgr.initSign(mPrivateKey)
            sgr.update(data)
            return sgr.sign()
        } catch (e: GeneralSecurityException) {
            throw RuntimeException(e)
        }
    }

    /**
     * Sign the provided data with the keypair's private key and returns [DecoratedSignature].
     * @param data
     */
    fun signDecorated(data: ByteArray): DecoratedSignature {
        return DecoratedSignature().apply {
            hint = this@KeyPair.signatureHint
            signature = org.stellar.sdk.xdr.Signature().apply { signature = this@KeyPair.sign(data)}
        }
    }

    /**
     * Verify the provided data and signature match this keypair's public key.
     * @param data The data that was signed.
     * @param signature The signature.
     * @return True if they match, false otherwise.
     * @throws RuntimeException
     */
    fun verify(data: ByteArray, signature: ByteArray): Boolean {
        return try {
            val sgr = EdDSAEngine(MessageDigest.getInstance("SHA-512"))
            sgr.initVerify(mPublicKey)
            sgr.update(data)
            sgr.verify(signature)
        } catch (e: SignatureException) {
            false
        } catch (e: GeneralSecurityException) {
            throw RuntimeException(e)
        }

    }

    companion object {

        private val ed25519 = EdDSANamedCurveTable.getByName("ed25519-sha-512")

        /**
         * Creates a new Stellar KeyPair from a strkey encoded Stellar secret seed.
         * @param seed Char array containing strkey encoded Stellar secret seed.
         * @return [KeyPair]
         */
        fun fromSecretSeed(seed: CharArray): KeyPair {
            val decoded = StrKey.decodeStellarSecretSeed(seed)
            val keypair = fromSecretSeed(decoded)
            Arrays.fill(decoded, 0.toByte())
            return keypair
        }

        /**
         * **Insecure** Creates a new Stellar KeyPair from a strkey encoded Stellar secret seed.
         * This method is <u>insecure</u>. Use only if you are aware of security implications.
         * @see [Using Password-Based Encryption](http://docs.oracle.com/javase/1.5.0/docs/guide/security/jce/JCERefGuide.html.PBEEx)
         *
         * @param seed The strkey encoded Stellar secret seed.
         * @return [KeyPair]
         */
        fun fromSecretSeed(seed: String): KeyPair {
            val charSeed = seed.toCharArray()
            val decoded = StrKey.decodeStellarSecretSeed(charSeed)
            val keypair = fromSecretSeed(decoded)
            Arrays.fill(charSeed, ' ')
            return keypair
        }

        /**
         * Creates a new Stellar keypair from a raw 32 byte secret seed.
         * @param seed The 32 byte secret seed.
         * @return [KeyPair]
         */
        fun fromSecretSeed(seed: ByteArray): KeyPair {
            val privKeySpec = EdDSAPrivateKeySpec(seed, ed25519)
            val publicKeySpec = EdDSAPublicKeySpec(privKeySpec.a.toByteArray(), ed25519)
            return KeyPair(EdDSAPublicKey(publicKeySpec), EdDSAPrivateKey(privKeySpec))
        }

        /**
         * Creates a new Stellar KeyPair from a strkey encoded Stellar account ID.
         * @param accountId The strkey encoded Stellar account ID.
         * @return [KeyPair]
         */
        fun fromAccountId(accountId: String): KeyPair {
            val decoded = StrKey.decodeStellarAccountId(accountId)
            return fromPublicKey(decoded)
        }

        /**
         * Creates a new Stellar keypair from a 32 byte address.
         * @param publicKey The 32 byte public key.
         * @return [KeyPair]
         */
        fun fromPublicKey(publicKey: ByteArray): KeyPair {
            val publicKeySpec = EdDSAPublicKeySpec(publicKey, ed25519)
            return KeyPair(EdDSAPublicKey(publicKeySpec))
        }

        /**
         * Generates a random Stellar keypair.
         * @return a random Stellar keypair.
         */
        fun random(): KeyPair {
            val keypair = KeyPairGenerator().generateKeyPair()
            return KeyPair(keypair.public as EdDSAPublicKey, keypair.private as EdDSAPrivateKey)
        }

        fun fromXdrPublicKey(key: PublicKey): KeyPair =KeyPair.fromPublicKey(key.ed25519.uint256)

        fun fromXdrSignerKey(key: SignerKey): KeyPair = KeyPair.fromPublicKey(key.ed25519.uint256)
    }
}
/**
 * Creates a new KeyPair without a private key. Useful to simply verify a signature from a
 * given public address.
 * @param publicKey
 */
