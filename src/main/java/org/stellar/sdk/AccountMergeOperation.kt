package org.stellar.sdk

import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.Operation.OperationBody
import org.stellar.sdk.xdr.OperationType

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [AccountMerge](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#account-merge) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class AccountMergeOperation private constructor(val destination: KeyPair) : Operation() {

    /**
     * The account that receives the remaining XLM balance of the source account.
     */

    internal override fun toOperationBody(): OperationBody {
        val destination = AccountID().apply { accountID = destination.xdrPublicKey }
        return OperationBody().apply {
            this.destination = destination
            this.discriminant = OperationType.ACCOUNT_MERGE
        }
    }

    /**
     * Builds AccountMerge operation.
     * @see AccountMergeOperation
     */
    class Builder {
        private val destination: KeyPair

        private var mSourceAccount: KeyPair? = null

        /**
         * Creates a new AccountMerge builder.
         * @param destination The account that receives the remaining XLM balance of the source account.
         */
        constructor(destination: KeyPair) {
            this.destination = destination
        }

        internal constructor(op: OperationBody) {
            destination = KeyPair.fromXdrPublicKey(op.destination.accountID!!)
        }

        /**
         * Set source account of this operation
         * @param sourceAccount Source account
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(sourceAccount: KeyPair): Builder {
            mSourceAccount = sourceAccount
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): AccountMergeOperation {
            val operation = AccountMergeOperation(destination)
            mSourceAccount?.let {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
