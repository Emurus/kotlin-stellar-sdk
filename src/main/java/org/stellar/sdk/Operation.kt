package org.stellar.sdk

import com.google.common.io.BaseEncoding;
import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.XdrDataOutputStream

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.math.BigDecimal

import com.google.common.base.Preconditions.checkNotNull
import org.stellar.sdk.xdr.OperationType

/**
 * Abstract class for operations.
 */
abstract class Operation internal constructor() {

    /**
     * Returns operation source account.
     */
    /**
     * Sets operation source account.
     * @param keypair
     */
    var sourceAccount: KeyPair? = null
        internal set(keypair) {
            field = checkNotNull<KeyPair>(keypair, "keypair cannot be null")
        }

    /**
     * Generates Operation XDR object.
     */
    fun toXdr(): org.stellar.sdk.xdr.Operation {
        val xdr = org.stellar.sdk.xdr.Operation()
        if (sourceAccount != null) {
            xdr.sourceAccount =  AccountID().apply { accountID = this@Operation.sourceAccount!!.xdrPublicKey}
        }
        xdr.body = toOperationBody()
        return xdr
    }

    /**
     * Returns base64-encoded Operation XDR object.
     */
    fun toXdrBase64(): String {
        try {
            val operation = this.toXdr()
            val outputStream = ByteArrayOutputStream()
            val xdrOutputStream = XdrDataOutputStream(outputStream)
            org.stellar.sdk.xdr.Operation.encode(xdrOutputStream, operation)
            val base64Encoding = BaseEncoding.base64()
            return base64Encoding.encode(outputStream.toByteArray());
        } catch (e: IOException) {
            throw AssertionError(e)
        }

    }

    /**
     * Generates OperationBody XDR object
     * @return OperationBody XDR object
     */
    internal abstract fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody

    companion object {

        private val ONE = BigDecimal(10).pow(7)

        fun toXdrAmount(value: String): Long {
            val amount = BigDecimal(value).multiply(Operation.ONE)
            return amount.longValueExact()
        }

        fun fromXdrAmount(value: Long): String {
            val amount = BigDecimal(value).divide(Operation.ONE)
            return amount.toPlainString()
        }

        /**
         * Returns new Operation object from Operation XDR object.
         * @param xdr XDR object
         */
        fun fromXdr(xdr: org.stellar.sdk.xdr.Operation): Operation {
            val body = xdr.body
            val operation = when (body.discriminant) {
                OperationType.CREATE_ACCOUNT -> CreateAccountOperation.Builder(body.createAccountOp).build()
                OperationType.PAYMENT -> PaymentOperation.Builder(body.paymentOp).build()
                OperationType.PATH_PAYMENT -> PathPaymentOperation.Builder(body.pathPaymentOp).build()
                OperationType.MANAGE_OFFER -> ManageOfferOperation.Builder(body.manageOfferOp).build()
                OperationType.CREATE_PASSIVE_OFFER -> CreatePassiveOfferOperation.Builder(body.createPassiveOfferOp).build()
                OperationType.SET_OPTIONS -> SetOptionsOperation.Builder(body.setOptionsOp).build()
                OperationType.CHANGE_TRUST -> ChangeTrustOperation.Builder(body.changeTrustOp).build()
                OperationType.ALLOW_TRUST -> AllowTrustOperation.Builder(body.allowTrustOp).build()
                OperationType.ACCOUNT_MERGE -> AccountMergeOperation.Builder(body).build()
                OperationType.MANAGE_DATA -> ManageDataOperation.Builder(body.manageDataOp).build()
                else -> throw RuntimeException("Unknown operation body " + body.discriminant)
            }
            if (xdr.sourceAccount != null) {
                operation.sourceAccount = KeyPair.fromXdrPublicKey(xdr.sourceAccount.accountID!!)
            }
            return operation
        }
    }
}
