package org.stellar.sdk


import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.Int64
import org.stellar.sdk.xdr.OperationType
import org.stellar.sdk.xdr.PaymentOp

import com.google.common.base.Preconditions.checkNotNull

/**
 * Represents [Payment](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#payment) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class PaymentOperation private constructor(val destination: KeyPair, val asset: Asset, val amount: String) : Operation() {

    override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val op = PaymentOp().apply {
            // destination
            destination = AccountID().apply { accountID = this@PaymentOperation.destination.xdrPublicKey }
            // asset
            asset = this@PaymentOperation.asset.toXdr()
            // amount
            amount = Int64().apply { int64 = Operation.toXdrAmount(this@PaymentOperation.amount) }
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.PAYMENT
            paymentOp = op
        }
    }

    /**
     * Builds Payment operation.
     * @see PathPaymentOperation
     */
    class Builder(private val destination: KeyPair, private val asset: Asset, private val amount: String) {

        private var mSourceAccount: KeyPair? = null

        /**
         * Construct a new PaymentOperation builder from a PaymentOp XDR.
         * @param op [PaymentOp]
         */
        internal constructor(op: PaymentOp):this(
                KeyPair.fromXdrPublicKey(op.destination.accountID!!),
                Asset.fromXdr(op.asset),
                Operation.fromXdrAmount(op.amount.int64!!.toLong())
        )

        /**
         * Sets the source account for this operation.
         * @param account The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(account: KeyPair): Builder {
            mSourceAccount = account
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): PaymentOperation {
            val operation = PaymentOperation(destination, asset, amount)
            mSourceAccount?.let {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
