package org.stellar.sdk

import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.AssetType

/**
 * Represents all assets with codes 5-12 characters long.
 * @see [Assets](https://www.stellar.org/developers/learn/concepts/assets.html)
 */
class AssetTypeCreditAlphaNum12
/**
 * Class constructor
 * @param code Asset code
 * @param issuer Asset issuer
 */
(code: String, issuer: KeyPair) : AssetTypeCreditAlphaNum(code, issuer) {

    override val type: String
        get() = "credit_alphanum12"

    init {
        if (code.length < 5 || code.length > 12) {
            throw AssetCodeLengthInvalidException()
        }
    }

    override fun toXdr(): org.stellar.sdk.xdr.Asset {
        val accountID = AccountID().apply { accountID = mIssuer.xdrPublicKey  }
        val credit = org.stellar.sdk.xdr.Asset.AssetAlphaNum12().apply {
            assetCode = Util.paddedByteArray(code, 12)
            issuer = accountID
        }
        val xdr = org.stellar.sdk.xdr.Asset().apply {
            discriminant = AssetType.ASSET_TYPE_CREDIT_ALPHANUM12
            alphaNum12 = credit
        }

        return xdr
    }
}
