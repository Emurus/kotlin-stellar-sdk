package org.stellar.sdk

import org.stellar.sdk.xdr.AccountID
import org.stellar.sdk.xdr.CreateAccountOp
import org.stellar.sdk.xdr.Int64
import org.stellar.sdk.xdr.OperationType

/**
 * Represents [CreateAccount](https://www.stellar.org/developers/learn/concepts/list-of-operations.html#create-account) operation.
 * @see [List of Operations](https://www.stellar.org/developers/learn/concepts/list-of-operations.html)
 */
class CreateAccountOperation private constructor(val destination: KeyPair, val startingBalance: String) : Operation() {

    internal override fun toOperationBody(): org.stellar.sdk.xdr.Operation.OperationBody {
        val destination = AccountID().apply { accountID = destination.xdrPublicKey  }
        val startingBalance = Int64().apply { int64 = Operation.toXdrAmount(startingBalance) }
        val op = CreateAccountOp().apply {
            this.destination = destination
            this.startingBalance = startingBalance
        }

        return org.stellar.sdk.xdr.Operation.OperationBody().apply {
            discriminant = OperationType.CREATE_ACCOUNT
            createAccountOp = op
        }
    }

    /**
     * Builds CreateAccount operation.
     * @see CreateAccountOperation
     */
    class Builder(private val destination: KeyPair, private val startingBalance: String) {
        private var mSourceAccount: KeyPair? = null

        /**
         * Construct a new CreateAccount builder from a CreateAccountOp XDR.
         * @param op [CreateAccountOp]
         */
        internal constructor(op: CreateAccountOp):this(
                KeyPair.fromXdrPublicKey(op.destination.accountID!!),
                Operation.fromXdrAmount(op.startingBalance.int64)
        )

        /**
         * Sets the source account for this operation.
         * @param account The operation's source account.
         * @return Builder object so you can chain methods.
         */
        fun setSourceAccount(account: KeyPair): Builder {
            mSourceAccount = account
            return this
        }

        /**
         * Builds an operation
         */
        fun build(): CreateAccountOperation {
            val operation = CreateAccountOperation(destination, startingBalance)
            mSourceAccount?.let {
                operation.sourceAccount = mSourceAccount
            }
            return operation
        }
    }
}
