package org.stellar.sdk;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class NetworkTest {
    @After
    public void resetNetwork() {
        Network.Companion.use(null);
    }

    @Test
    public void testNoDefaultNetwork() {
        assertNull(Network.Companion.current());
    }

    @Test
    public void testSwitchToTestNetwork() {
        Network.Companion.useTestNetwork();
        assertEquals("Test SDF Network ; September 2015", Network.Companion.current().getNetworkPassphrase());
    }

    @Test
    public void testSwitchToPublicNetwork() {
        Network.Companion.usePublicNetwork();
        assertEquals("Public Global Stellar Network ; September 2015", Network.Companion.current().getNetworkPassphrase());
    }
}
