package org.stellar.sdk;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AccountTest  {

  @Test
  public void testGetIncrementedSequenceNumber() {
    Account account = new Account(KeyPair.Companion.random(), 100L);
    Long incremented;
    incremented = account.getIncrementedSequenceNumber();
    assertEquals(100L, account.getSequenceNumber());
    assertEquals(new Long(101L), incremented);
    incremented = account.getIncrementedSequenceNumber();
    assertEquals(100L, account.getSequenceNumber());
    assertEquals(new Long(101L), incremented);
  }

  @Test
  public void testIncrementSequenceNumber() {
    Account account = new Account(KeyPair.Companion.random(), 100L);
    account.incrementSequenceNumber();
    assertEquals(account.getSequenceNumber(), 101L);
  }

  @Test
  public void testGetters() {
    KeyPair keypair = KeyPair.Companion.random();
    Account account = new Account(keypair, 100L);
    assertEquals(account.getKeypair().getAccountId(), keypair.getAccountId());
    assertEquals(account.getSequenceNumber(), 100L);
  }
}